//
//  ForeksTableViewHeader.swift
//  ForeksDemo
//
//  Created by Kadircan Türker on 24.09.2020.
//  Copyright © 2020 Kadircan Türker. All rights reserved.
//

import UIKit

class ForeksTableViewHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var fieldsTextField: UITextField!
    @IBOutlet weak var symbolTextField: UITextField!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
