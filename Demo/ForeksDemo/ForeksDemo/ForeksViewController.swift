//
//  ForeksViewController.swift
//  ForeksDemo
//
//  Created by Kadircan Türker on 23.09.2020.
//  Copyright © 2020 Kadircan Türker. All rights reserved.
//

import UIKit

internal class ForeksViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var tableView: UITableView!
    private var dataSource = ForeksDataSource()
    private var viewModel: ForeksViewModel!
    weak var timer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        
        viewModel = ForeksViewModel(networkManager: BaseNetwork())
        startFetching()
        viewModel.completionHandler = { [weak self] in
            guard let self = self else { return }
            self.dataSource.model = self.viewModel.cellModels
            self.tableView.reloadData()
        }
        // Do any additional setup after loading the view.
    }

    deinit {
        stopTimer()
    }
}

private extension ForeksViewController {
    func configureTableView() {
        dataSource.registerCells(to: tableView)
        tableView.dataSource = dataSource
    }
    
    func startFetching() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [weak self] _ in
            guard let self = self else { return }
            self.viewModel.fetchList()
        }
    }

    func stopTimer() {
        timer?.invalidate()
    }
}

extension ForeksViewController: UITableViewDelegate {
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 1
//    }
//
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        return viewModel.symbolsList?.count ?? 0
//    }
//
//    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        if let symbols = viewModel.symbolsList {
//            return symbols[row].name
//        } else {
//            return ""
//        }
//    }

//    func pickerView( pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        theTextField.text = myPickerData[row]
//    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "tableviewHeader") as! ForeksTableViewHeader
        headerView.fieldsTextField.delegate = self
        headerView.symbolTextField.delegate = self
        headerView.titleLabel.text = "Symbols"
        let pickerView = UIPickerView()
        pickerView.accessibilityIdentifier = "fields"
        headerView.fieldsTextField.inputView = pickerView
               
        let symbolPickerView = UIPickerView()
        symbolPickerView.accessibilityIdentifier = "symbol"
        headerView.symbolTextField.inputView = symbolPickerView
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
}

