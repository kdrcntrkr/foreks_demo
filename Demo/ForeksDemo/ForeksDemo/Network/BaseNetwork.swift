//
//  BaseNetwork.swift
//  ForeksDemo
//
//  Created by Kadircan Türker on 23.09.2020.
//  Copyright © 2020 Kadircan Türker. All rights reserved.
//

import Foundation
import Alamofire


internal class BaseNetwork {
    internal func sendRequest<T: Decodable>(with requestAPI: ForeksAPI, responseType: T.Type, completionHandler: @escaping (T)->Void) {
        AF.request(requestAPI.path, method: requestAPI.method, parameters: requestAPI.parameters)
        .validate(statusCode: 200..<300)
        .validate(contentType: ["application/json"])
        .responseDecodable(of: T.self) { (response) in
          print(response)
            switch response.result {
            case .success:
                if let value = response.value {
                    completionHandler(value)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
}

internal enum ForeksAPI {
    case getList
    case getDetail(String, String)
}

extension ForeksAPI {
    internal var path: String {
        switch self {
        case .getList:
            return "https://sui7963dq6.execute-api.eu-central-1.amazonaws.com/default/ForeksMobileInterviewSettings"
        case .getDetail:
            return "https://sui7963dq6.execute-api.eu-central-1.amazonaws.com/default/ForeksMobileInterview"
        }
    }
    
    internal var parameters: Dictionary<String, String> {
        switch self {
        case .getDetail(let fields, let symbols):
            return ["fields": fields, "stcs": symbols]
        default:
            return [:]
        }
    }
    
    internal var method: HTTPMethod {
        return .get
    }
}
internal enum Foreks {
    internal struct ListModel: Codable {
        internal let mypageDefaults: [MyPageDefaultsItem]
        internal let mypage: [MyPageItem]
    }
    
    internal struct MyPageDefaultsItem: Codable {
        internal let cod: String
        internal let gro: String
        internal let tke: String
        internal let def: String
    }
    
    
    internal struct MyPageItem: Codable {
        internal let name: String
        internal let key: String
    }

    internal struct DetailListModel: Codable {
        internal let l: [ListItem]
        internal let z: String
    }

    internal struct ListItem: Codable {
        internal let tke: String
        internal let clo: String
        internal let pdd: String
        internal let las: String
    }
}

