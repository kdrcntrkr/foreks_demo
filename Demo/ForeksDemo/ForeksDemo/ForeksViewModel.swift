//
//  ForeksViewModel.swift
//  ForeksDemo
//
//  Created by Kadircan Türker on 24.09.2020.
//  Copyright © 2020 Kadircan Türker. All rights reserved.
//

import Foundation
import UIKit

internal class ForeksViewModel {
    
    private var networkManager: BaseNetwork?
    internal var completionHandler: (() -> ())?
    private var listModel: Foreks.ListModel?
    private var detailListModel: Foreks.DetailListModel?
    private var keys: String?
    private var symbols: String?
    internal var isSymbolsChanged: Bool = false
    internal var cellModels: [ForeksTableViewCell.Model]?
    internal var symbolsList: [Foreks.MyPageItem]?
    
    init(networkManager: BaseNetwork) {
        self.networkManager = networkManager
    }

    func fetchList() {
        networkManager?.sendRequest(with: .getList, responseType: Foreks.ListModel.self, completionHandler: { [weak self] (response) in
            guard let self = self else { return }
            self.listModel = response
            self.prepareKeys()
            self.prepareSymbols()
            self.fetchDetails()
        })
    }
}

private extension ForeksViewModel {
    func fetchDetails() {
        guard let keys = keys, let symbols = symbols else { return }
        networkManager?.sendRequest(with: .getDetail(symbols, keys), responseType: Foreks.DetailListModel.self, completionHandler: { [weak self] (response) in
            guard let self = self else { return }
            self.checkValues(with: response)
        })
    }
    
    func checkValues(with newData: Foreks.DetailListModel) {
        guard let cellModels = cellModels else {
            self.createCellModel(with: newData)
            return
            
        }
        
        
        self.cellModels = newData.l.map( { (item) -> ForeksTableViewCell.Model in
            
            if let index = cellModels.firstIndex(where: {$0.tke == item.tke }) {
                let model = cellModels[index]
                
                let oldClock = Int(model.clock.replacingOccurrences(of: ":", with: "")) ?? 0
                let newClock = Int(item.clo.replacingOccurrences(of: ":", with: "")) ?? 0
                
                let oldVal = Int(model.value.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "")) ?? 0
                let newVal = Int(item.las.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "")) ?? 0
                
                var valPos: ForeksTableViewCell.ValuePosition = .noChange
                if oldVal < newVal {
                    valPos = .positive
                } else if oldVal > newVal {
                    valPos = .negative
                }
                
                return ForeksTableViewCell.Model(tke: model.tke, title: model.title, clock: item.clo, value: item.las, change: item.pdd, isNeededHighlighted: oldClock < newClock ? true : false, valuePosition: valPos)
            } else {
                return ForeksTableViewCell.Model(tke: "", title: "", clock: "", value: "", change: "", isNeededHighlighted: false, valuePosition: .noChange)
            }
            
            
        })
        self.completionHandler?()
    }
    
    func createCellModel(with model: Foreks.DetailListModel) {
        
        guard let mypageDefaults = listModel?.mypageDefaults else { return }
        
        cellModels = model.l.map( { (item) -> ForeksTableViewCell.Model in
            if let index = mypageDefaults.firstIndex(where: { $0.tke == item.tke }) {
                return ForeksTableViewCell.Model(tke: item.tke, title: mypageDefaults[index].cod, clock: item.clo, value: item.las, change: item.pdd, isNeededHighlighted: false, valuePosition: .noChange)
            } else {
                return ForeksTableViewCell.Model(tke: "", title: "", clock: "", value: "", change: "", isNeededHighlighted: false, valuePosition: .noChange)
            }
        })
    }
    
    func prepareKeys() {
        guard let listModel = listModel else { return }
        let keyArray = listModel.mypageDefaults.map { $0.tke }
        keys = keyArray.joined(separator: "~")
    }
    
    func prepareSymbols() {
        guard let listModel = listModel, listModel.mypage.count > 1, !isSymbolsChanged else { return }
        symbolsList = listModel.mypage
        symbols = "\(listModel.mypage[0].key),\(listModel.mypage[1].key)"
    }
}
