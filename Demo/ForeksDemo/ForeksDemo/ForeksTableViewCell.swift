//
//  ForeksTableViewCell.swift
//  ForeksDemo
//
//  Created by Kadircan Türker on 23.09.2020.
//  Copyright © 2020 Kadircan Türker. All rights reserved.
//

import UIKit

class ForeksTableViewCell: UITableViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

internal extension ForeksTableViewCell {
    struct Model {
        internal let tke: String
        internal let title: String
        internal var clock: String
        internal let value: String
        internal let change: String
        internal let isNeededHighlighted: Bool
        internal let valuePosition: ValuePosition
        internal var changeColor: UIColor {
            return change.contains("-") ? .red : .green
        }


        public init(tke: String, title: String, clock: String, value: String, change: String, isNeededHighlighted: Bool, valuePosition: ValuePosition) {
            self.tke = tke
            self.title = title
            self.clock = clock
            self.value = value
            self.change = change
            self.isNeededHighlighted = isNeededHighlighted
            self.valuePosition = valuePosition
        }
    }

    enum Configurator {
        public static func configure(cell: ForeksTableViewCell, with model: Model) {
            cell.titleLabel.text = model.title
            cell.subtitleLabel.text = model.clock
            cell.valueLabel.text = model.value
            cell.statusLabel.text = model.change
            if model.isNeededHighlighted {
                cell.mainView.backgroundColor = .black
            } else {
                cell.mainView.backgroundColor = .darkGray
            }
            cell.statusLabel.textColor = model.changeColor
            switch model.valuePosition {
            case .positive:
                cell.statusView.backgroundColor = .green
            case .negative:
                cell.statusView.backgroundColor = .red
            default:
                cell.statusView.backgroundColor = .clear
            }
        }
    }
    
    enum ValuePosition {
        case noChange, positive, negative
    }
}
