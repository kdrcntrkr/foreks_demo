//
//  ForeksDataSource.swift
//  ForeksDemo
//
//  Created by Kadircan Türker on 23.09.2020.
//  Copyright © 2020 Kadircan Türker. All rights reserved.
//

import UIKit

class ForeksDataSource: NSObject, UITableViewDataSource, TableViewDataSourceProtocol {
    
    internal var model: [ForeksTableViewCell.Model]?
    
    internal func registerCells(to tableView: UITableView) {
        let nib = UINib(nibName: "ForeksTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "foreksCell")
        let nibs = UINib(nibName: "ForeksTableViewHeader", bundle: Bundle.main)
        tableView.register(nibs, forHeaderFooterViewReuseIdentifier: "tableviewHeader")
    }

    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model?.count ?? 0
    }

    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "foreksCell", for: indexPath) as! ForeksTableViewCell
        guard let model = model else { return cell }
        ForeksTableViewCell.Configurator.configure(cell: cell, with: model[indexPath.row])
        return cell
    }
}

public protocol TableViewDataSourceProtocol: UITableViewDataSource {
    func registerCells(to tableView: UITableView)
}
